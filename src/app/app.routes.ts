import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AboutComponent } from './about.component';
import { EduComponent } from './education.component';

export const routes: Routes = [
    {path: 'about', component: AboutComponent},
    {path: 'education', component: EduComponent}
]

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);